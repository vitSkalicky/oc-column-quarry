local component = require("component")
local computer = require("computer")
local robot = require("robot")
local shell = require("shell")
local sides = require("sides")
local math = require("math")
local inventory = component.inventory_controller

local NEEDS_CHARGE_THRESHOLD = 0.2
local FULL_CHARGE_THRESHOLD = 0.95
local TOOL_MIN_DURABILITY = 0.1
local MIN_ENERGY = 0.3
local alwaysChangeTool = false

local args, options = shell.parse(...)

if args[1] == "help" or not component.isAvailable("robot") or not component.isAvailable("inventory_controller") then
  io.write("\n" .. 
"NAME\n" .. 
"  column_quarry - Faster infinite quarry\n" .. 
"       \n" .. 
"REQUIREMENTS\n" .. 
"  Must run on a robot with Inventory controller \n" .. 
"  upgrade. Optional Chunkloader upgrade is\n" .. 
"  recommended.\n" .. 
"\n" .. 
"SYNOPSIS\n" .. 
"  column_quarry [OPTIONS]...\n" .. 
"    Starts mining.\n" .. 
"         \n" .. 
"  column_quarry help\n" .. 
"    Displays this help\n" .. 
"\n" .. 
"OPTIONS\n" .. 
"  -c, --continue\n" .. 
"    Resumes the quarry progress on the last mined\n" .. 
"    column. Useful for example if the robot got\n" .. 
"    stuck and you had to break it. Just place the\n" .. 
"    robot at the starting point, use this option\n" .. 
"    and it will continue where it was.\n" .. 
"\n" .. 
"  --continue=n\n" .. 
"    Where *n* is the side of the already mined\n" .. 
"    area. The robot will continue mining roughly\n" .. 
"    at the end of the entered area. Useful if\n" .. 
"    replacing a lost robot. If you want to replace\n" .. 
"     a robot you did not lose, it is better copy\n" .. 
"     the state file (see *State saving*)\n" .. 
"\n" .. 
"  -a, --alwaysChangeTool\n" .. 
"    The robot will change its primary tool\n" .. 
"    (pickaxe) every time it visits the start\n" .. 
"    point, not only when it is almost broken.\n" .. 
"    Recommended when using a repairable tool like\n" .. 
"    in the example setups.\n" .. 
"           \n" .. 
"HELP\n" .. 
"  Detailed guide on using this program can be\n" .. 
"  found on\n" .. 
"  https://gitlab.com/vitSkalicky/oc-column-quarry\n" .. 
"  ")
  
  if not component.isAvailable("robot") or not component.isAvailable("inventory_controller") then
    io.stderr:write("can only run on robots with an inventory controller upgrade\n")
  end
  return
end

if component.isAvailable("chunkloader") then
  component.chunkloader.setActive(true)
end

local selSlot = 1
local additionalTools = 0

local r = component.robot
local x, y, z, f = 0, 0, 0, 0
local dropping = false -- avoid recursing into drop()
local delta = {[0] = function() x = x + 1 end, [1] = function() y = y + 1 end,
               [2] = function() x = x - 1 end, [3] = function() y = y - 1 end} -- records move in certain direction
local column = 0
local miningDown = true --from which direction are we mining the column
local traverseZ = -1 --at what z are we moving from ono column to another when down (when up we use z=0)

local function turnRight()
  robot.turnRight()
  f = (f + 1) % 4
end

local function turnLeft()
  robot.turnLeft()
  f = (f - 1) % 4
end

local function turnTowards(side)
  if f == side - 1 then
    turnRight()
  else
    while f ~= side do
      turnLeft()
    end
  end
end


local function swingWithAllTools(side) --swing using all the different additional tools
  local result, reason = r.swing(side)
  for i = 0, additionalTools - 1 do
    local slot = r.inventorySize() - i
    r.select(slot)
    inventory.equip()
    result, reason = r.swing(side)
    inventory.equip()
    if result then
      r.select(1)
      return result
    end
  end
  r.select(1)
  return false
end

local function clearBlock(side, cannotRetry) --return false if fails and cannot retry
  while r.suck(side) do end
  local result, reason = r.swing(side)
  if not result then
    local _, what = r.detect(side)
    
    if cannotRetry and what ~= "air" and what ~= "entity" then
      return false
    end
    if reason == "block" then
      return swingWithAllTools(side)
    end
  end
  return true
end

local function tryMove(side, digFirst) --returns false if fails (10 tries on removing obstacle failed)
  side = side or sides.forward
  local tries = 10
  if digFirst then
    clearBlock(side)
  end
  while not r.move(side) do
    tries = tries - 1
    if not clearBlock(side, tries < 1) then
      io.write("could not move in direction " .. side .. " on x: " .. x .. " y: " .. y .." z(up/down): " .. z .. "\n")
      return false
    end
  end
  if side == sides.down then
    z = z + 1
  elseif side == sides.up then
    z = z - 1
  else
    delta[f]()
  end
  return true
end

local function moveTo(tx, ty, tz, backwards)
  local axes = {
    function()
      while z > tz do
        tryMove(sides.up)
      end
      while z < tz do
        tryMove(sides.down)
      end
    end,
    function()
      if y > ty then
        turnTowards(3)
        repeat tryMove() until y == ty
      elseif y < ty then
        turnTowards(1)
        repeat tryMove() until y == ty
      end
    end,
    function()
      if x > tx then
        turnTowards(2)
        repeat tryMove() until x == tx
      elseif x < tx then
        turnTowards(0)
        repeat tryMove() until x == tx
      end
    end
  }
  if backwards then
    for axis = 3, 1, -1 do
      axes[axis]()
    end
  else
    for axis = 1, 3 do
      axes[axis]()
    end
  end
end

local function needsNewTool()
  local ret = not robot.durability() or robot.durability() < TOOL_MIN_DURABILITY
  if (ret) then
    io.write("Tool duarbility is getting low: " .. (robot.durability() or "nil") .." < " .. TOOL_MIN_DURABILITY .. "\n")
  end
  return ret

end

local function isInventoryFull()
  local inventorySize = robot.inventorySize()
  local ret = robot.count(inventorySize - 1 - additionalTools) ~= 0 --rather sooner 
  if ret then
    io.write("inventory is almost full" .. "\n")
  end
  return ret
end
local function isEnergyLow()
  local ret = computer.energy() / computer.maxEnergy() < MIN_ENERGY
  if (ret) then
    io.write("energy is low: " .. computer.energy() / computer.maxEnergy() .." < " .. MIN_ENERGY .. "\n")
  end
  return ret
end
local function needsNewAdditionalTool()
  for i = 0, additionalTools - 1 do
    local slot = r.inventorySize() - i
    local itemStack = inventory.getStackInInternalSlot(slot)
    if itemStack then
      local dmg = itemStack.damage
      local maxdmg = itemStack.maxDamage
      local durability = 1 - dmg/maxdmg
      if durability < TOOL_MIN_DURABILITY or maxdmg - dmg < 3 then
        return true
      end
    else
      return true
    end
  end
  return false
end
local function shouldDropOff() -- returns true if the robot should dropOff
  return not dropping and (needsNewTool() or isEnergyLow() or isInventoryFull() or needsNewAdditionalTool())
end

local function dropOff() -- drops off and stay on start
  while (shouldDropOff()) do
    io.write("returning to drop off..." .. "\n")
    dropping = true

    moveTo(0, 0, 0)
    turnTowards(2)

    local inventorySize = robot.inventorySize()

    for slot = 1, inventorySize - additionalTools do
      if robot.count(slot) > 0 then
        robot.select(slot)
        local wait = 1
        repeat
          if not robot.drop() then
            os.sleep(wait)
            wait = math.min(10, wait + 1)
          end
        until robot.count(slot) == 0
      end
    end
    robot.select(1)

    dropping = false

    --wait for recharge
    while computer.energy() / computer.maxEnergy() < 0.9 do
      os.sleep(5)
    end

    --change tool
    if (robot.durability() ~= 1 and alwaysChangeTool) or needsNewTool() then
      moveTo(0,1,0)
      turnTowards(2)
      robot.select(1)
      robot.suck()
      inventory.equip()
      moveTo(0,1,-1)
      turnTowards(2)
      robot.drop()
    end

    --change additional tools
    --drop almost broken ones
    for i = 0, additionalTools - 1 do
      local slot = inventorySize - i
      local itemStack = inventory.getStackInInternalSlot(slot)
      if itemStack then
        local dmg = itemStack.damage
        local maxdmg = itemStack.maxDamage
        local durability = 1 - dmg/maxdmg
        if durability < TOOL_MIN_DURABILITY or maxdmg - dmg < 3 then
          moveTo(0,2 + i,-1)
          turnTowards(2)
          robot.select(slot)
          robot.drop()
        end
      end
    end
    --take new ones
    additionalTools = 0
    while true do
      local slot = inventorySize - additionalTools
      if robot.count(slot) == 0 then
        moveTo(0,2 + additionalTools,0)
        turnTowards(2)
        robot.select(slot)
        robot.suck()
        if robot.count(slot) == 0 then
          break
        end
      end
      additionalTools = additionalTools + 1
    end

    robot.select(1)
    moveTo(0, 0, 0)
    turnTowards(0)
  end
end

-- up to here it is mostly general navigation

--[[
The robot mines in this pattern:

    ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _
    _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _
    _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■
    _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _
    _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _
    ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _
    _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _
    _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■
    _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _
    _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _
    ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _
    _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _
    _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■
    _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _
    _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _

On every black square ■ the robot mines down/up spinning around and mining all 4
 adjacent blocks. In this pattern, every block has one black square next to it, 
 so all blocks will be mined. I call the places where the robot mines down/up
 (marked by black squares) "mining columns".

The space is mined in "groups" of columns. Each group consists of 5 columns in a
 way that groups can tile.

One group:


    X
    ↓
    6 _ □ _ ↓ _ _ □
    5 _ _ → ■ ← ↓ _
    4 □ _ ↓ ↑ → ■ ←
    3 _ → ■ ← ↓ ↑ _ □
    2 _ ↓ ↑ → ■ ← _
    1 → ■ ← _ ↑ _ □
    0 _ ↑ _ □ _ _ _
      0 1 2 3 4 5 6 ← Y

* ■ - "mining column" - Here the robot mines down/up spinning and mining all
  adjacent blocks (marked by arrows)
* arrows - this block is mined by an adjacent column
* _ - ignored
* □ - columns of the next/previous group

The footprint of a group looks like this:

          ■
        ■ ■ ■ ■ 
        ■ ■ ■ ■ ■
      ■ ■ ■ ■ ■ 
      ■ ■ ■ ■ ■
    ■ ■ ■   ■
      ■

If you look close enough, you see, that these shapes can tile vertically and
 horizontally to mine the entire space.

The robot always mines one group and then moves on to the next one. Groups in
space are ordered like this:

    X
    ↓    ...
    25   35 34 33 32 31 30
    20   16 17 18 19 20 29
    15   15 14 13 12 21 28
    10    4  5  6 11 22 27
     5    3  2  7 10 23 26
     0    0  1  8  9 24 25 ...
 
          0  5 10 15 20 25 ← Y
          
Drawn with arrows:

    X
    ↓    ...
    25    ↑  ←  ←  ←  ←  ←
    20    →  →  →  →  ↓  ↑
    15    ↑  ←  ←  ←  ↓  ↑
    10    →  →  ↓  ↑  ↓  ↑
     5    ↑  ←  ↓  ↑  ↓  ↑
     0    →  ↑  →  ↑  →  ↑ ...
 
          0  5 10 15 20 25 ← Y
 
... and continues into infinity.
]]

-- This function takes an index of columns and calculates its XY position. It adds 1
--  to the coordinates, so that the robot does not mine under the charger and
--  chests. Columns are indexed from 0.
local function getColumnLocation(i)
  --get group location
  local groupI = math.floor(i / 5)

  local gx, gy

  local b = math.floor(math.sqrt(groupI))
  local d = groupI - b^2
  if d <= b then
    gx = b
    gy = d
  else
    gx = 2*b - d
    gy = b
  end
  if b % 2 == 1 then
    local tmp = gy
    gy = gx
    gx = tmp
  end

  gx = gx * 5
  gy = gy * 5

  local inGroupPositions = {{0,0},{2,1},{4,2},{3,4},{1,3}}

  local iloc = inGroupPositions[i % 5 + 1] --don't forget that lua indexes from 1

  return gx + iloc[1] + 1, gy + iloc[2] + 1 --here I add one so that the robot does not mine under the starting point
end

-- if side == nil, the robot won't move
local function mineStep(side)
  clearBlock(sides.forward)
  turnRight()
  clearBlock(sides.forward)
  turnRight()
  clearBlock(sides.forward)
  turnRight()
  clearBlock(sides.forward)

  if side == nil then
    return true
  end

  return tryMove(side, true)
end

local function mineColumn(mineDown, skipFromZ) --assumes the robot is already on the starting point
  if mineDown then
    while z ~= skipFromZ and mineStep(sides.down) do
      if shouldDropOff() then
        --drop off and return to continue mining
        local rx, ry, rz = x, y, z
        dropOff()
        moveTo(rx, ry, rz, true)
        mineColumn(true)
        return
      end
    end
    if z == skipFromZ then
      while tryMove(sides.down) do end
    end
    if traverseZ == -1 then
      traverseZ = z - 4 -- to avoid bedrock
    end
    return moveTo(x,y, traverseZ)
  else
    --mining from down up
    local sz = z
    while mineStep(sides.down) do end
    if traverseZ == -1 then
      traverseZ = z - 4
    end
    moveTo(x,y,sz - 1)
    while z > 0 do
      if not mineStep(sides.up) then
        return false
      end
      if shouldDropOff() then
        --return to start using last column, then drop off and continue mining this column, but from up.
        local rx, ry, rz = x, y, z
        --we use the previous column to go up
        local lx, ly = getColumnLocation(column - 1)
        moveTo(lx, ly, z, true)
        dropOff()
        moveTo(rx, ry, 0, true)
        miningDown = true
        --also when minig down, skip the part from botton that is already mined mined 
        mineColumn(true, rz + 1)
        return
      end
    end
    mineStep(nil)
    return true
  end
end

local function finish()
  io.write("Finishing" .. "\n")
  dropOff(true)
  moveTo(0, 0, 0)
  turnTowards(0)
end

local function mineLoop()
  dropOff()
  miningDown = true
  while true do
    local columnX, columnY = getColumnLocation(column)
    moveTo(columnX, columnY, z)
    mineColumn(miningDown)
    column = column + 1
    miningDown = not miningDown
    --save state
    file=io.open("column_quarry_state","w")
    file:write(column)
    file:close()
  end
end

if type(options.continue) == "string" then
  column = ( (math.floor((tonumber(options.continue) / 5)) - 1)^2)*5
else
  if options.continue or options.c then
    file=io.open("column_quarry_state","r")
    if file then
      contents=tonumber(file:read())
      column = contents or 0
      file:close()
    end
  end
end


if options.alwaysChangeTool or options.a then
  alwaysChangeTool = true
end

mineLoop()
