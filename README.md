# Faster infinite quarry for [OpenComputers](https://github.com/MightyPirates/OpenComputers) Minecraft mod

This is program for robot quarry. It uses a special pattern thanks to which it is up to **40% faster than the default dig.lua**. The pattern also stretches into infinity, so if you equip the robot with a chunk loader upgrade you never have to worry about it again.

The robot also remembers its position, so it the quarry can be easily resumed.
 
Usage
-----

1. Assemble a robot (see *Requirements* below).
2. Set up power, tool supply, etc. (see *Setup* below).
3. Place the robot and turn it on.
4. Copy the [minified program][1] (squashed onto a single line).
5. In the robot type `edit column_quarry.lua` and paste the program (by clicking the mouse wheel or using a keybind).
6. Press `ctrl`+`s` to save the program and `ctrl`+`w` to exit editor.
7. Run the program by typing `column_quarry`. See *Program options* below.
 
Requirements
------------

### Minimal

![screenshot](min requirements.png)

* Computer case tier 2
* CPU tier 1
* 2x memory tier 1
* EPPROM **with Lua BIOS**
* Hard disk tier 1 with OpenOS installed*
* Screen tier 1
* Keyboard
* Graphics card tier 1
* Inventory controller upgrade
* Inventory upgrade (more is better)
* Hover upgrade **

*use a computer to install OpenOS on the disk before assembling the robot, or add a disk drive to the robot (like in the screenshot).

**If you use Hove upgrade tier 1, you **must** start mining at **y < 64**, otherwise the robot will get stuck in the air. If you use tier 2 you can mine at any height.

Also note that the minimal requirements robot does not have a Chunk loader upgrade, therefore you have to load the chunks externally (by using ChickenChunks chunk loader for example)

### Recommended

![screenshot](rec requirements.png)

* Computer case tier 3
* CPU tier 1
* 2x memory tier 1
* EPPROM **with Lua BIOS**
* Hard disk tier 1 with OpenOS installed*
* Screen tier 1
* Keyboard
* Graphics card tier 1
* Inventory controller upgrade
* Chunkloader upgrade
* Battery upgrade
* Hover upgrade *
* Inventory upgrade (more is better)
* upgrade container for more inventory upgrades

*see *Minimal requirements*

Setup
-----

Schema:

    C R
      I P A A...
      
* R - Robot facing the top of schema
* C - Charger
* I - Items chest: the robot will put all mined blocks and resources here
* P - Pickaxes: robot will take fresh primary mining tools from here and will put used (almost broken) ones to a chest *on top of it*
* A - Additional tools: robot will take one tool from each chest in row. Almost broken ones will be put to a chest above. It will use these to try breaking blocks it cannot break with pickaxe (or the primary tool you supply it with). More on that later.
* The robot mines area to the top right of the schema

You can use any tool instead of pickaxe (such as a mining drill from IC2) and any inventory for items, not only chest. For example: the robot takes a charged mining drill from a batbox and it puts the discharged one into a hopper above the batbox, which pushes the discharged drill into the batbox for recharging.

The robot **cannot mine cobwebs using a pickaxe** (and other blocks from mods such as Magic bees hives). That's what additional tools are for. If a robot fails to mine a block using pickaxe, it will try to mine it using all the available additional tools.

The robot takes one additional tool from each chest to the right of the "pickaxe" chest. You can put as many chest you want - the robot counts how many chests there are.
  
Screenshot:
![screenshot](setup screenshot.png)

* The robot is facing away from the ender chest
* The charger is powered by a creative capacitor bank and is turned on by a lever. You can use almost any type of power, but for some you will need a power converter.
* Ender chest - mined resources will be put here
* Iron chest - robot will take fresh primary mining tools (pickaxes) from here
* Golden chest - used (almost broken) pickaxes will be put here
* Copper chests - robot will take one additional tool from each. There are three, but you can have more, less or even none.
* Diamond chest - used (almost broken) additional tools will be put here (to the one above the chest the tool was taken from)
* Red area will be mined (continues infinitely)

### Mid-game setup on Direwolf 1.12.2

![setup screenshot](setup real 1.png)

* I use diamond drill (IC2) instead of pickaxe, because it can be recharged.
* I also use a chainsaw (IC2)(rechargable too) for cobwebs and scoop for bee hives (Magic bees)
* Power is supplied using a geothermal generator with an ender tank hooked up to my lava pump in the nether.
* Ender chest sends mined items back to my base
* The robot will take a charged drill and chainsaw from the batbox and put the used one into the hopper which pushes the tool back to the batbox to recharge.
* The robot takes a charged drill first and then returns the used one, so I need two of them. One is being used by the robot and the other one is charging. (same for chainsaw)
* Scoops cannot be easily repaired, so I just have a ton of them.
* Don't forget to load the chunk with a spot loader and use a chunk loader upgrade in the robot.
* I use the `--alwaysChangeTool` flag so that the robot recharges the drill whenever it drops off item.

### End-game setup on Direwolf20 1.12.2

![real setup 2](setup real 2.png)

* It is located in the Twilight forest dimension, because the *y* is lower in it (there is less stone to be mined before we get to the diamonds) and the solar cell works non-stop.
* I use EnderIO solar cell with a capacitor for power.
* There is another ender chest in my base which suck up items into my storage system and ore processing factory.
* The robot mines with an Efficiency V, Fortune III, Unbreaking III, **Mending** diamond pickaxe. I also use 2 additional tools: sword for cobwebs and scoop for bee hives (from Magic bees mod). Both have Mending on them as well.
* Tools are repaired in the *EnderIO fluid tanks* using *liquid XP* (tools have mending on them). I get the *liquid XP* from my mob farm by converting *essence* (Industrial foregoing) from *mob crusher* in *fluid dictionary converter* and pumping it through *ender tank*.
* There are always 2 identical tools of the same kind: one is being used by the robot and the other one is waiting repaired to be exchanged for the almost broken one.
* The robot takes a fresh tool from the *fluid tank* and puts the used tool into the *hopper*, which pushes it into the *fluid tank*, where it is repaired.

Program options
---------------

### Usage

    column_quarry [OPTIONS]...
    column_quarry help
    
### Arguments

    help
    
Displays help instead of starting the quarry.
    
### Options

    --continue
    
Resumes the quarry progress on the last mined column. Useful for example if the robot got stuck and you had to break it. Just place the robot at the starting point, use this option and it will continue where it was.

    --continue=n
    
Where *n* is the side of the already mined area. The robot will continue mining **roughly** at the end of the entered area. Useful if replacing a lost robot. If you want to replace a robot you did not lose, it is better copy the state file (see *State saving*)

    --alwaysChangeTool
    
The robot will change its primary tool (pickaxe) every time it visits the start point, not only when it is almost broken. **Recommended when using a repairable tool** like in the example setups.

State saving
------------

The robot saves its progress every time it finished mining a column into file named `column_quarry_state`. If you want to replace the robot, just copy the contents of this file onto the new robot and start the quarry on the new robot with the `--continue` option. I will continue, where the old robot finished.

Download
--------

Copy and paste the program in either human-readable or minified version into the robot. Due to the 256 line paste limit, you will have to paste the program in parts or use the minified version, which is squashed onto a single line.

Latest version download:

* [human-readable version][2]
* [minified version][1]

How it works
------------

The robot mines in this pattern:

    ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _
    _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _
    _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■
    _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _
    _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _
    ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _
    _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _
    _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■
    _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _
    _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _
    ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _
    _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _
    _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■
    _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _
    _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _ _ _ _ ■ _

On every black square ■ the robot mines down/up spinning around and mining all 4 adjacent blocks. In this pattern, every block has one black square next to it, so all blocks will be mined. I call the places where the robot mines down/up (marked by black squares) "mining columns".

The space is mined in "groups" of columns. Each group consists of 5 columns in a way that groups can tile.

One group:


    X
    ↓
    6 _ □ _ ↓ _ _ □
    5 _ _ → ■ ← ↓ _
    4 □ _ ↓ ↑ → ■ ←
    3 _ → ■ ← ↓ ↑ _ □
    2 _ ↓ ↑ → ■ ← _
    1 → ■ ← _ ↑ _ □
    0 _ ↑ _ □ _ _ _
      0 1 2 3 4 5 6 ← Y

* ■ - "mining column" - Here the robot mines down/up spinning and mining all adjacent blocks (marked by arrows)
* arrows - this block is mined by an adjacent column
* _ - ignored
* □ - columns of the next/previous group

The footprint of a group looks like this:

          ■
        ■ ■ ■ ■ 
        ■ ■ ■ ■ ■
      ■ ■ ■ ■ ■ 
      ■ ■ ■ ■ ■
    ■ ■ ■   ■
      ■

If you look carefully, you see that these shapes can tile vertically and horizontally to mine the entire space.

The robot always mines one group and then moves on to the next one. Groups in space are ordered like this:

    X
    ↓    ...
    25   35 34 33 32 31 30
    20   16 17 18 19 20 29
    15   15 14 13 12 21 28
    10    4  5  6 11 22 27
     5    3  2  7 10 23 26
     0    0  1  8  9 24 25 ...
 
          0  5 10 15 20 25 ← Y
          
Drawn with arrows:

    X
    ↓    ...
    25    ↑  ←  ←  ←  ←  ←
    20    →  →  →  →  ↓  ↑
    15    ↑  ←  ←  ←  ↓  ↑
    10    →  →  ↓  ↑  ↓  ↑
     5    ↑  ←  ↓  ↑  ↓  ↑
     0    →  ↑  →  ↑  →  ↑ ...
 
          0  5 10 15 20 25 ← Y
 
... and continues into infinity.

[1]: https://vitskalicky.gitlab.io/oc-column-quarry/column_quarry.min.lua.txt
[2]: https://gitlab.com/vitSkalicky/oc-column-quarry/-/blob/master/column_quarry.lua
